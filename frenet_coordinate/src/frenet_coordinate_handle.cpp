#include <ros/ros.h>
#include "frenet_coordinate_handle.hpp"
#include "fsd_common_msgs/Map.h"
#include <time.h>
#include <iomanip>
#include <vector>
#include "Eigen/Dense"
#include <fstream>
#include <iostream>




namespace ns_frenet_coordinate
{

    // Constructor
    FrenetCoordinateHandle::FrenetCoordinateHandle(ros::NodeHandle &nodeHandle) : nodeHandle_(nodeHandle)
                                                                                  //boundaryDetector_(nodeHandle)
    {
        ROS_INFO("Constructing Handle");
        loadParameters();
        subscribeToTopics();
        publishToTopics();
        srvCall();
    }

    // Getters
    int FrenetCoordinateHandle::getNodeRate() const { return node_rate_; }

    // Methods
    void FrenetCoordinateHandle::loadParameters()
    {
        ROS_INFO("loading handle parameters");
        if (!nodeHandle_.param<std::string>("global_map_topic_name",
                                            global_map_topic_name_,
                                            "/estimation/slam/map"))
        {
            ROS_WARN_STREAM("Did not load global_map_topic_name. Standard value is: " << global_map_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("slam_map_topic_name",
                                        slam_map_topic_name_,
                                        "/estimation/slam/map_fssim")) {
        ROS_WARN_STREAM("Did not load slam_map_topic_name. Standard value is: " << slam_map_topic_name_);
    }
        if (!nodeHandle_.param<std::string>("gps_centerline_topic_name",
                                            gps_centerline_topic_name_,
                                            "/control/pure_pursuit/asensing_global_path"))
        {
            ROS_WARN_STREAM("Did not load global_map_topic_name. Standard value is: " << gps_centerline_topic_name_);
        }
        if (!nodeHandle_.param("node_rate", node_rate_, 5))
        {
            ROS_WARN_STREAM("Did not load node_rate. Standard value is: " << node_rate_);
        }
        if (!nodeHandle_.param<std::string>("slam_state_topic_name",
                                        slam_state_topic_name_,
                                        "/estimation/slam/state")) {
        ROS_WARN_STREAM("Did not load slam_state_topic_name. Standard value is: " << slam_state_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("frenet_coordinate_topic_name",
                                        frenet_coordinate_topic_name_,
                                        "/planning/global_center_path")) {
        ROS_WARN_STREAM("Did not load slam_state_topic_name. Standard value is: " << frenet_coordinate_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("frame_id",
                                        frame_id_,
                                        "fssim/vehicle/base_link ")) {
        ROS_WARN_STREAM("Did not load frame_id. Standard value is: " << frame_id_);
        }
        if (!nodeHandle_.param<std::string>("frenet_coordinate_compute_time_topic_name",
                                            frenet_coordinate_compute_time_topic_name_,
                                            "/frenet_coordinate_compute_time"))
        {
            ROS_WARN_STREAM("Did not load visual_path_topic_name. Standard value is: " << frenet_coordinate_compute_time_topic_name_);
        }
        if (!nodeHandle_.param<double>("evaluation_threshold", 
                                        evaluation_threshold_,
                                        1.0))
        {
            ROS_WARN_STREAM("Did not load evaluation_threshold. Standard value is: " << evaluation_threshold_);
        }
        if (!nodeHandle_.param<double>("dis_evaluation_weight", 
                                        dis_evaluation_weight_, 
                                        0.5)) 
        {
            ROS_WARN_STREAM("Did not load dis_evaluation_weight. Standard value is: " << dis_evaluation_weight_);
        }
        if (!nodeHandle_.param<double>("centre_evaluation_weight", 
                                        centre_evaluation_weight_, 
                                        0.5)) 
        {
            ROS_WARN_STREAM("Did not load centre_evaluation_weight. Standard value is: " << centre_evaluation_weight_);
        }
        if (!nodeHandle_.param<double>("discrete_dis", 
                                        discrete_dis_, 
                                        2.5)) 
        {
            ROS_WARN_STREAM("Did not load discrete_dis. Standard value is: " << discrete_dis_);
        }
        if (!nodeHandle_.param<int>("jump_points", 
                                    jump_points_,
                                    5)) 
        {
            ROS_WARN_STREAM("Did not load jump_points. Standard value is: " << jump_points_);
        }
        if (!nodeHandle_.param<double>("record_dis", 
                                        record_dis_, 
                                        0.01)) 
        {
            ROS_WARN_STREAM("Did not load record_dis. Standard value is: " << record_dis_);
        }
        if (!nodeHandle_.param<double>("start_dis", 
                                        start_dis_, 
                                        10.0)) 
        {
            ROS_WARN_STREAM("Did not load start_dis. Standard value is: " << start_dis_);
        }
        if (!nodeHandle_.param<double>("end_dis", 
                                        end_dis_, 
                                        0.3)) 
        {
            ROS_WARN_STREAM("Did not load end_dis. Standard value is: " << end_dis_);
        }
        if (!nodeHandle_.param<double>("change_angle_rate", 
                                        change_angle_rate_, 
                                        0.8)) 
        {
            ROS_WARN_STREAM("Did not load change_angle_rate. Standard value is: " << change_angle_rate_);
        }
        if (!nodeHandle_.param<double>("change_reference_rate", 
                                        change_reference_rate_, 
                                        1)) 
        {
            ROS_WARN_STREAM("Did not load change_reference_rate. Standard value is: " << change_reference_rate_);
        }
    }

    void FrenetCoordinateHandle::subscribeToTopics()
    {
        ROS_INFO("subscribe to topics");
        globalMapSubscriber = nodeHandle_.subscribe(global_map_topic_name_, 1, &FrenetCoordinateHandle::globalMapCallback, this);
        GpsSubscriber = nodeHandle_.subscribe(gps_centerline_topic_name_, 1, &FrenetCoordinateHandle::gpscenterlineCallback_new, this);
        //slamStateSubscriber_  = nodeHandle_.subscribe(slam_state_topic_name_, 1, &FrenetCoordinateHandle::slamStateCallback, this);
        // fssim
        //slamMapSubscriber_   = nodeHandle_.subscribe(slam_map_topic_name_, 1, &FrenetCoordinateHandle::slamMapCallback, this);
    }

    void FrenetCoordinateHandle::publishToTopics()
    {
        ROS_INFO("publish to topics");
        FrenetDetectionsPublisher = nodeHandle_.advertise<nav_msgs::Path>(frenet_coordinate_topic_name_, 1);
        FilteredPath = nodeHandle_.advertise<nav_msgs::Path>("filtered_path", 1);
        Visual_Center_Line = nodeHandle_.advertise<visualization_msgs::MarkerArray>("visual_center_line", 1);
        Visual_Discrete_Map = nodeHandle_.advertise<visualization_msgs::MarkerArray>("visual_discrete_map", 1);
        //Discrete_Map = nodeHandle_.advertise<fsd_common_msgs::ConeDetections>("discrete_map", 1);
        //frenet_coordinate_compute_time_Publisher = nodeHandle_.advertise<std_msgs::double32>(frenet_coordinate_compute_time_topic_name_, 1);
        Map_Left = nodeHandle_.advertise<fsd_common_msgs::ConeDetections>("map_left", 1);
        Map_Right = nodeHandle_.advertise<fsd_common_msgs::ConeDetections>("map_right", 1);
        Visual_Map_Right = nodeHandle_.advertise<visualization_msgs::MarkerArray>("visual_map_right", 1);
        Visual_Map_Left = nodeHandle_.advertise<visualization_msgs::MarkerArray>("visual_map_left", 1);
        Visual_Temp_1 = nodeHandle_.advertise<visualization_msgs::MarkerArray>("Visual_Temp_1", 1);
        Visual_Temp_2 = nodeHandle_.advertise<visualization_msgs::MarkerArray>("Visual_Temp_2", 1);
        width_pub = nodeHandle_.advertise<frenet_coordinate::RoadWidth>("road_width", 1);
    }

    void FrenetCoordinateHandle::srvCall()
    {
        ROS_INFO("call srv");
        // convert_s_client_ = nodeHandle_.serviceClient<convert_s::serviceData>("/control/tool/convert_s");
        //velocity_planning_client_ = nodeHandle_.serviceClient<velocity_planning::velocity_planning>(velocity_planning_srv_name_);
    }

    void FrenetCoordinateHandle::slamStateCallback( const fsd_common_msgs::CarState &state)
    {
        setState(state);
    }//TODO
    void FrenetCoordinateHandle::setState(const fsd_common_msgs::CarState &state) 
    {
        state_ = state;
    }



    void FrenetCoordinateHandle::run()
    {
        if(get_gps) FilteredPath.publish(gps_path_filtered);     
        if(get_cone && get_gps && !sign_finish)
        {
            gettimeofday(&start_time, NULL);
            runAlgorithm();
            gettimeofday(&end_time, NULL);
            const auto compute_time = static_cast<float>((end_time.tv_sec * 1000.0 + end_time.tv_usec / 1000.0) - 
                                                    (start_time.tv_sec * 1000.0 + start_time.tv_usec / 1000.0));
            frenet_coordinate_compute_time.data = compute_time;
            std::cout << "Comepute time" << frenet_coordinate_compute_time.data << std::endl;
            //frenet_coordinate_compute_time_Publisher.publish(frenet_coordinate_compute_time);
        }

        if(sign_finish)
        {
            Visual_Center_Line.publish(marker_array_center_line);
            Visual_Discrete_Map.publish(marker_array_discrete_line);

            //Discrete_Map.publish(map_for_ribbon);

            global_center_path.header.frame_id = frame_id_;
            FrenetDetectionsPublisher.publish(global_center_path);

            width_pub.publish(road_width);

            Map_Left.publish(map_for_publish_left);
            Map_Right.publish(map_for_publish_right);

            Visual_Map_Right.publish(marker_array_map_right);
            Visual_Map_Left.publish(marker_array_map_left);
            Visual_Temp_1.publish(Temp_1);
            Visual_Temp_2.publish(Temp_2);
        }
    }



    void FrenetCoordinateHandle::runAlgorithm()
    {  
        route(gps_center_path);
        for(const auto &point :cone_global_disorder_coordinate.polygon.points)
        {
            double x = point.x;
            double y = point.y;
            find_rs(x,y);
        }
        ROS_INFO("finish frenet transmation");

        cal_center_path_frenet();

        //将中心线转换成cartesian坐标
        int match_index_rs = 0;
        for(const auto & t : global_center_line_frenet.polygon.points)
        {
            double min_r = 9999;
            for( int k = match_index_rs; k < gps_center_path.poses.size(); k++)
            {
                double dr = fabs(t.x - gps_center_path.poses[k].pose.position.z);
                if(dr < min_r)
                {
                    min_r = dr;
                    match_index_rs = k;
                }
                else
                {
                    frenet_to_cartesian(gps_center_path.poses[match_index_rs].pose.position.x, 
                                        gps_center_path.poses[match_index_rs].pose.position.y,
                                        gps_center_path.poses[match_index_rs].pose.orientation.z, 
                                        gps_center_path.poses[match_index_rs].pose.position.z, 
                                        t.y);
                    break;
                }
            }
        }
        
        // std::ofstream write;
        // write.open("/home/auto-lp/map.txt");
        // for(int i = 0;i<global_center_path.poses.size();i++ )
        // {
        //     write << global_center_path.poses[i].pose.position.x << '\t';
        //     write << global_center_path.poses[i].pose.position.y << '\t';
        //     write << "\n";
        //     std::cout << global_center_path.poses[i].pose.position.x << '\t' << global_center_path.poses[i].pose.position.y << std::endl;
        // }
        // std::cout << "Finish write" << std::endl;

        deal_global_center_path(1);
        std::cout << "global_center_path: " << global_center_path.poses.size() << std::endl;

        //基于中心线进行两侧离散
        discrete_center_line();

        //对地图进行评价
        evaluate_data = evaluate_discrete_map();
        if(evaluate_data == 0)
        {
            map_for_publish_left = map_for_ribbon_left;
            map_for_publish_right = map_for_ribbon_right;
        }
        else
        {
            map_for_publish_left = map_from_slam_left_ordered;
            map_for_publish_right = map_from_slam_right_ordered;
        }

        //MarkerArray可视化左右车道
        for (const auto & cone : map_for_publish_left.cone_detections)
        {
            visiual_marker_array_CUBE(marker_array_map_left, "world", "r", cone.position.x, cone.position.y, cone.position.z);
        }
        for (const auto & cone : map_for_publish_right.cone_detections)
        {
            visiual_marker_array_CUBE(marker_array_map_right, "world", "b", cone.position.x, cone.position.y, cone.position.z);
        }

        //MarkerArray可视化global_center_path
        global_center_path_TO_visiual_marker_array_CUBE();
        
        // std::ofstream write;
        // write.open("/home/marco/AMZ/src/center_line.txt");
        // for(int i = 0;i<global_center_path.poses.size();i++ )
        // {
        //     write << global_center_path.poses[i].pose.position.x << '\t';
        //     write << global_center_path.poses[i].pose.position.y << '\t';
        //     write << "\n";
        //     // std::cout << global_center_path.poses[i].pose.position.x << '\t' << global_center_path.poses[i].pose.position.y << std::endl;
        // }
        sign_finish = true;
    }


  void FrenetCoordinateHandle::globalMapCallback(const fsd_common_msgs::Map &msg)
    {
        if(!get_cone )
        {
            map_from_slam_left.cone_detections = msg.cone_red;
            map_from_slam_right.cone_detections = msg.cone_blue;
            // map_from_slam.cone_detections = msg.cone_red;
            // map_from_slam.cone_detections.insert(map_from_slam.cone_detections.end(),msg.cone_blue.begin(),msg.cone_blue.end());
            
            for (size_t i = 0; i < msg.cone_red.size(); i++)
            {
                fsd_common_msgs::Cone cone;
                cone.position.x = msg.cone_red[i].position.x;
                cone.position.y = msg.cone_red[i].position.y;
                map_from_slam.cone_detections.push_back(cone); 
            }
            for (size_t i = 0; i < msg.cone_blue.size(); i++)
            {
                fsd_common_msgs::Cone cone;
                cone.position.x = msg.cone_red[i].position.x;
                cone.position.y = msg.cone_red[i].position.y;
                map_from_slam.cone_detections.push_back(cone); 
            }

            int msg_size1 = msg.cone_red.size(), msg_size2 = msg.cone_blue.size();
            std::cout << "msg_size1: " << msg_size1 << ", msg_size2: " << msg_size2 << std::endl;
            double x_g[msg_size1 + msg_size2] = {}, y_g[msg_size1 + msg_size2] = {};
            for (size_t i = 0; i < msg_size1; i++)
            {
                x_g[i] = msg.cone_red[i].position.x;
                y_g[i] = msg.cone_red[i].position.y;
            }
            for (size_t i = 0; i < msg_size2; i++)
            {
                x_g[i + msg_size1] = msg.cone_blue[i].position.x;
                y_g[i + msg_size1] = msg.cone_blue[i].position.y;
            }

            for(size_t i = 0; i < (msg_size1 + msg_size2); i++)
            {
                geometry_msgs::Point32 t;
                // t.x = x_g[i] + 22.2200411219;
                // t.y = y_g[i] - 26.375358041;
                t.x = x_g[i];
                t.y = y_g[i];
                cone_global_disorder_coordinate.polygon.points.push_back(t);
                std::cout << "i: " << i << ", cone" << ": "<< x_g[i] << ',' << y_g[i] << std::endl;
            }
            // std::cout << cone_global_disorder_coordinate.polygon.points.size() << std::endl;
            if(msg_size1 != 0 && msg_size2 != 0)
            {
                get_cone = true;
                std::cout << "fet_cone" << std::endl;
            }
            
            // std::cout << get_cone << std::endl;

            // std::cout << "start" << std::endl;
            // std::cout  << "size:" << '\t';
            // std::cout << cone_global_disorder_coordinate.polygon.points.size() << std::endl;
            // geometry_msgs::PolygonStamped detect_cone;
            // detect_cone.polygon.points.clear();
            // // for(const auto &cone : cone_global_disorder_coordinate.polygon.points)
            // // {
            // //     std::cout << "已存点" << '\t' << cone.x << '\t' << cone.y <<std::endl;
            // // }
            // for(const auto &cone : msg.cone_detections)
            // {
            //     double d = sqrt(pow(cone.position.x,2) + pow(cone.position.y, 2));
            //     if(d < 10.0)
            //     {
            //         geometry_msgs::Point32 p;
            //         p.x = cone.position.x *cos (state_.car_state.theta) - cone.position.y* sin (state_.car_state.theta) + state_.car_state.x;
            //         p.y = cone.position.x * sin (state_.car_state.theta) + cone.position.y * cos (state_.car_state.theta) +state_.car_state.y;
            //         // std::cout << "cone"  << '\t'<< p.x << '\t' << p.y <<std::endl;
            //         detect_cone.polygon.points.push_back(p);
            //     }
            // }

            // int sign_close = 1;
            // // std::cout << "cone_size" << std::endl;
            
            // if (cone_global_disorder_coordinate2.polygon.points.size() == 0)
            // {
            //     cone_global_disorder_coordinate2 = detect_cone;
            //     head_pp.x = cone_global_disorder_coordinate2.polygon.points[0].x;
            //     head_pp.y = cone_global_disorder_coordinate2.polygon.points[0].y;
            // }
            // else
            // {
            //     geometry_msgs::PolygonStamped temp_cone;
            //     temp_cone.polygon.points.clear();
            //     for(int i = 0; i < detect_cone.polygon.points.size(); i++)
            //     {
            //         double min_d = 10;
            //         for(int j =  0;j < cone_global_disorder_coordinate2.polygon.points.size(); j++)
            //         {
            //             double d = sqrt(pow(detect_cone.polygon.points[i].x - cone_global_disorder_coordinate2.polygon.points[j].x, 2)
            //                                             + pow(detect_cone.polygon.points[i].y - cone_global_disorder_coordinate2.polygon.points[j].y, 2));
            //             // std::cout << d << std::endl;
            //             if(d < min_d)
            //             {
            //                 min_d = d;
            //                 if(cone_global_disorder_coordinate2.polygon.points.size()  > 30 && sign_close !=0)
            //                 {
            //                     double d_close = sqrt(pow(head_pp.x - detect_cone.polygon.points[i].x, 2)
            //                                             + pow(head_pp.y - detect_cone.polygon.points[i].y, 2));
            //                     if(d_close < 1)
            //                     {
            //                         // std::cout << "j" << '\t' << j<< std::endl;
            //                         sign_close = j;
            //                     }
                                
            //                 }
            //             }
            //         }
            //         // std::cout << "i" << i <<"min_d" << min_d <<std::endl;
            //         if(min_d > 1.5)
            //         {
            //             // std::cout<< "as" <<std::endl;
            //             geometry_msgs::Point32 pp;
            //             pp.x = detect_cone.polygon.points[i].x;
            //             pp.y = detect_cone.polygon.points[i].y;
            //             temp_cone.polygon.points.push_back(pp);
            //         }
            //     }

            //     for(const auto &cone : temp_cone.polygon.points)
            //     {
            //         geometry_msgs::Point32 t;
            //         t.x = cone.x;
            //         t.y = cone.y;
            //         cone_global_disorder_coordinate2.polygon.points.push_back(t);
            //     }
            // }

            // if(sign_close == 0)
            // {
            //     iter_count += 1;
            //     if(iter_count > 3)
            //     {
            //         get_cone = true;
            //         std::cout << "finish cone" << std::endl;
            //          for(const auto &cone : cone_global_disorder_coordinate2.polygon.points)
            //         {
            //             // std::cout << "----------------" << std::endl;
            //             std::cout << "已存点" << '\t' << cone.x << '\t' << cone.y <<std::endl;
            //         }
            //     }
            //     std::ofstream myout("/home/m/cone.txt");
            //     for(const auto &cone : cone_global_disorder_coordinate2.polygon.points)
            //     {
            //         myout<< cone.x << '\t' << cone.y  << std::endl;
            //     }
            //     myout.close();
            // }

            // for(const auto &cone : cone_global_disorder_coordinate.polygon.points)
            // {
            //     std::cout << "cone: "<< cone.x << '\t' << cone.y <<std::endl;
            // }
        }
    }

    void FrenetCoordinateHandle::slamMapCallback(const fsd_common_msgs::Map &map)
    {
        std::cout << "sadf" << std::endl;
        if(!get_cone )
        {
            for (const auto &yellow: map.cone_red)
            {
                geometry_msgs::Point32 pp;
                pp.x = yellow.position.x;
                pp.y = yellow.position.y;
                cone_global_disorder_coordinate.polygon.points.push_back(pp);
            }

            for (const auto &blue: map.cone_blue)
            {
                geometry_msgs::Point32 pp;
                pp.x = blue.position.x;
                pp.y = blue.position.y;
                cone_global_disorder_coordinate.polygon.points.push_back(pp);
            }

            std::cout << "get_cone" << std::endl;
            get_cone = true;
        }
    }   

    // void FrenetCoordinateHandle::gpscenterlineCallback(const nav_msgs::Path &gpscenterpath)
    // {
    //     int gps_num = gpscenterpath.poses.size();
    //     gps_center_path.poses.clear();
    //     if(!get_gps && gps_num > 0)
    //     {
    //         // std::cout << "dsfsdf" << std::endl;

    //         // for(int i = 0; i < gps_num; i++)
    //         // {
    //         //     std::cout << gpscenterpath.poses[i].pose.position.x << '\t' << gpscenterpath.poses[i].pose.position.y << std::endl;
    //         // }

    //         // gps_center_path = gpscenterpath;

    //         for(int i = 0; i < gps_num; i++)
    //         {
    //             gps_center_path.poses[i].pose.position.x = gpscenterpath.poses[i].pose.position.x;
    //             gps_center_path.poses[i].pose.position.y = gpscenterpath.poses[i].pose.position.y;
    //             gps_center_path.poses[i].pose.orientation.z = gpscenterpath.poses[i].pose.orientation.z;
    //         }

    //         get_gps = true;
    //         std::cout << "123";

    //         // for(const auto &p:gps_center_path.poses)
    //         // {
    //         //     std::cout << "gps" << '\t'<< p.pose.position.x << '\t' << p.pose.position.y << std::endl;
    //         // }
    //     }
    // }

    void FrenetCoordinateHandle::gpscenterlineCallback_new(const fsd_common_msgs::CarState &gpscenterpath)
    {
        if(get_gps)
        {
            return;
        }

        geometry_msgs::PoseStamped p;
        double delta_dis = 0.0;
        if(flag_gps_num == 0)
        {
            gps_path_filtered.header.frame_id = "world";
            p.pose.position.x = gpscenterpath.car_state.x;
            p.pose.position.y = gpscenterpath.car_state.y;
            gps_path_filtered.poses.push_back(p);
            flag_gps_num = 1;
            return;
        }

        delta_dis = sqrt(pow(gpscenterpath.car_state.x - gps_path_filtered.poses[gps_path_filtered.poses.size()-1].pose.position.x,2)+
                         pow(gpscenterpath.car_state.y - gps_path_filtered.poses[gps_path_filtered.poses.size()-1].pose.position.y,2));
        
        if(delta_dis < record_dis_)
        {
            goto SHOW;
        }
        odom_dis += delta_dis;
        p.pose.position.x = gpscenterpath.car_state.x;
        p.pose.position.y = gpscenterpath.car_state.y;
        gps_path_filtered.poses.push_back(p);

SHOW:   int gps_path_filtered_path_size = gps_path_filtered.poses.size();
        double start_end_dis = sqrt(pow(gps_path_filtered.poses[0].pose.position.x-
                                gps_path_filtered.poses[gps_path_filtered.poses.size()-1].pose.position.x,2)+
                                pow(gps_path_filtered.poses[0].pose.position.y-
                                gps_path_filtered.poses[gps_path_filtered.poses.size()-1].pose.position.y,2));

        std::cout << "get_gps: " << get_gps << ", get_cone: " << get_cone << std::endl;
        std::cout << "gps_path_filtered_path_size: " << gps_path_filtered_path_size << std::endl;
        std::cout << "delta_dis: " << delta_dis << ", odom_dis: " << odom_dis << std::endl;
        std::cout << "init: [" << gps_path_filtered.poses[0].pose.position.x << "," 
                               << gps_path_filtered.poses[0].pose.position.y << "]" << std::endl;
        std::cout << "current: [" << gps_path_filtered.poses[gps_path_filtered_path_size-1].pose.position.x << "," 
                                  << gps_path_filtered.poses[gps_path_filtered_path_size-1].pose.position.y << "]" << std::endl;
        
        if(odom_dis > start_dis_ && !get_gps)
        {
            // for(int i = 0; i < gpscenterpath.poses.size()-1;i++)
            // {
            //     gps_center_path.poses[i].pose.position.x = gpscenterpath.poses[i].pose.position.x;
            //     gps_center_path.poses[i].pose.position.y = gpscenterpath.poses[i].pose.position.y;
            //     gps_center_path.poses[i].pose.orientation.z = gpscenterpath.poses[i].pose.orientation.z;
            // }
            
            std::cout << "start_end_dis: " << start_end_dis << std::endl;
            if(start_end_dis < end_dis_)
            {
                get_gps = true;
                std::cout << "==================================" << std::endl;
                ROS_WARN_STREAM("Have gotten gps. gps points: " << gps_path_filtered_path_size);
                std::cout << "==================================" << std::endl;
                gps_center_path = gps_path_filtered;

                //Calculate orientation
                for (size_t i = 0; i < gps_path_filtered_path_size-1; i++)
                {
                    double yaw,yaw_temp,
                            x1 = gps_center_path.poses[i].pose.position.x,
                            x2 = gps_center_path.poses[i+1].pose.position.x,
                            y1 = gps_center_path.poses[i].pose.position.y,
                            y2 = gps_center_path.poses[i+1].pose.position.y;
                    yaw = atan2(y2-y1,x2-x1);
                    //std::cout << "[" << x2-x1 << "," << y2-y1 << "]\t\tyaw: " << yaw << std::endl;
                    gps_center_path.poses[i].pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0,0,yaw);
                }
                gps_center_path.poses[gps_path_filtered_path_size-1].pose.orientation = gps_center_path.poses[gps_path_filtered_path_size-2].pose.orientation;

                // for( auto &p_temp:gps_center_path.poses)
                // {
                //     std::cout << "center_path: [" << p_temp.pose.position.x << ","
                //                                  << p_temp.pose.position.y << "," 
                //                                  << p_temp.pose.position.z << "," 
                //                                  << p_temp.pose.orientation.x << "," 
                //                                  << p_temp.pose.orientation.y << "," 
                //                                  << p_temp.pose.orientation.z << ","
                //                                  << p_temp.pose.orientation.w << "]" << std::endl;
                // }
                get_gps = true;
            }
            // geometry_msgs::Point32 top_p;
            // geometry_msgs::Point32 last_p;
            // top_p.x = gpscenterpath.poses[0].pose.position.x;
            // top_p.y = gpscenterpath.poses[0].pose.position.y;
            // last_p.x = gpscenterpath.poses[gps_num-1].pose.position.x;
            // last_p.y = gpscenterpath.poses[gps_num-1].pose.position.y;
            // double dis = sqrt(pow(top_p.x - last_p.x, 2) +pow(top_p.y - last_p.y, 2));

            // if(dis < 0.3)
            // {
            //     get_gps = true;
            //     std::cout << "finish gps"  << gps_num << std::endl;
            //     gps_center_path = gpscenterpath;
            //     for( auto &p:gps_center_path.poses)
            //     {
            //         p.pose.position.z = 0;
            //         p.pose.orientation.x = 0;
            //         p.pose.orientation.y = 0;
            //         // std::cout << p.pose.position.x << '\t' << p.pose.position.y << std::endl;
            //     }
            // }
        } 
        std::cout << "--------------------" << std::endl;
    }

    void FrenetCoordinateHandle::gpscenterlineCallback(const nav_msgs::Path &gpscenterpath)
    {
        if(get_gps)
        {
            return;
        }

        geometry_msgs::PoseStamped p;
        double delta_dis = 0.0;
        if(flag_gps_num == 0)
        {
            gps_path_filtered.header.frame_id = "world";
            flag_gps_num = gpscenterpath.poses.size();
            gps_num = 1;
            p.pose.position = gpscenterpath.poses[0].pose.position;
            p.pose.orientation = gpscenterpath.poses[0].pose.orientation;
            gps_path_filtered.poses.push_back(p);
        }
        if((gpscenterpath.poses.size() - flag_gps_num + 1 - gps_num) > jump_points_)
        {
            delta_dis = sqrt(pow(gpscenterpath.poses[gpscenterpath.poses.size()-1].pose.position.x-
                                gps_path_filtered.poses[gps_path_filtered.poses.size()-1].pose.position.x,2)+
                                pow(gpscenterpath.poses[gpscenterpath.poses.size()-1].pose.position.y-
                                gps_path_filtered.poses[gps_path_filtered.poses.size()-1].pose.position.y,2));
            if(delta_dis < record_dis_)
            {
                goto SHOW;
            }
            gps_num = gpscenterpath.poses.size() - flag_gps_num + 1;
            p.pose.position = gpscenterpath.poses[gpscenterpath.poses.size()-1].pose.position;
            p.pose.orientation = gpscenterpath.poses[gpscenterpath.poses.size()-1].pose.orientation;
            gps_path_filtered.poses.push_back(p);
            odom_dis += delta_dis;
        }

SHOW:   int gps_path_filtered_path_size = gps_path_filtered.poses.size();
        double start_end_dis = sqrt(pow(gps_path_filtered.poses[0].pose.position.x-
                                gps_path_filtered.poses[gps_path_filtered.poses.size()-1].pose.position.x,2)+
                                pow(gps_path_filtered.poses[0].pose.position.y-
                                gps_path_filtered.poses[gps_path_filtered.poses.size()-1].pose.position.y,2));
        
        std::cout << "gps_num: " << gps_num << "  get_gps: " << get_gps << std::endl;
        std::cout << "gps_path_filtered_path_size: " << gps_path_filtered_path_size << std::endl;
        std::cout << "delta_dis: " << delta_dis << ", odom_dis: " << odom_dis << std::endl;
        std::cout << "current: [" << gps_path_filtered.poses[gps_path_filtered_path_size-1].pose.position.x << "," 
                                  << gps_path_filtered.poses[gps_path_filtered_path_size-1].pose.position.y << "]" << std::endl;
        
        if(odom_dis > start_dis_ && !get_gps)
        {
            // for(int i = 0; i < gpscenterpath.poses.size()-1;i++)
            // {
            //     gps_center_path.poses[i].pose.position.x = gpscenterpath.poses[i].pose.position.x;
            //     gps_center_path.poses[i].pose.position.y = gpscenterpath.poses[i].pose.position.y;
            //     gps_center_path.poses[i].pose.orientation.z = gpscenterpath.poses[i].pose.orientation.z;
            // }
            
            std::cout << "start_end_dis: " << start_end_dis << std::endl;
            if(start_end_dis < end_dis_)
            {
                get_gps = true;
                std::cout << "==================================" << std::endl;
                ROS_WARN_STREAM("Have gotten gps. gps points: " << gps_path_filtered_path_size);
                std::cout << "==================================" << std::endl;
                gps_center_path = gps_path_filtered;

                //Calculate orientation
                for (size_t i = 0; i < gps_path_filtered_path_size-1; i++)
                {
                    double yaw,yaw_temp,
                            x1 = gps_center_path.poses[i].pose.position.x,
                            x2 = gps_center_path.poses[i+1].pose.position.x,
                            y1 = gps_center_path.poses[i].pose.position.y,
                            y2 = gps_center_path.poses[i+1].pose.position.y;
                    yaw = atan2(y2-y1,x2-x1);
                    //std::cout << "[" << x2-x1 << "," << y2-y1 << "]\t\tyaw: " << yaw << std::endl;
                    gps_center_path.poses[i].pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(0,0,yaw);
                }
                gps_center_path.poses[gps_path_filtered_path_size-1].pose.orientation = gps_center_path.poses[gps_path_filtered_path_size-2].pose.orientation;

                // for( auto &p_temp:gps_center_path.poses)
                // {
                //     std::cout << "center_path: [" << p_temp.pose.position.x << ","
                //                                  << p_temp.pose.position.y << "," 
                //                                  << p_temp.pose.position.z << "," 
                //                                  << p_temp.pose.orientation.x << "," 
                //                                  << p_temp.pose.orientation.y << "," 
                //                                  << p_temp.pose.orientation.z << ","
                //                                  << p_temp.pose.orientation.w << "]" << std::endl;
                // }
                get_gps = true;
            }
            // geometry_msgs::Point32 top_p;
            // geometry_msgs::Point32 last_p;
            // top_p.x = gpscenterpath.poses[0].pose.position.x;
            // top_p.y = gpscenterpath.poses[0].pose.position.y;
            // last_p.x = gpscenterpath.poses[gps_num-1].pose.position.x;
            // last_p.y = gpscenterpath.poses[gps_num-1].pose.position.y;
            // double dis = sqrt(pow(top_p.x - last_p.x, 2) +pow(top_p.y - last_p.y, 2));

            // if(dis < 0.3)
            // {
            //     get_gps = true;
            //     std::cout << "finish gps"  << gps_num << std::endl;
            //     gps_center_path = gpscenterpath;
            //     for( auto &p:gps_center_path.poses)
            //     {
            //         p.pose.position.z = 0;
            //         p.pose.orientation.x = 0;
            //         p.pose.orientation.y = 0;
            //         // std::cout << p.pose.position.x << '\t' << p.pose.position.y << std::endl;
            //     }
            // }
        } 
        std::cout << "--------------------" << std::endl;
    }

    void FrenetCoordinateHandle::route(const nav_msgs::Path &gps_center_path_)
    {
        double rs_ = 0;
        double k_ ;
        double rx_;
        double ry_;
        double rtheta_;
        for(int i = 0; i < gps_center_path_.poses.size(); i++)
        {
            rx_ = gps_center_path_.poses[i].pose.position.x;
            ry_ = gps_center_path_.poses[i].pose.position.y;

            if(i  == 0)
            {
                rtheta_ = gps_center_path_.poses[i+1].pose.orientation.z;
                gps_center_path.poses[i].pose.orientation.z = rtheta_;
            }

            if(i != 0)
            {
                rs_ += std::hypot(rx_ - gps_center_path_.poses[i-1].pose.position.x, ry_ - gps_center_path_.poses[i-1].pose.position.y);
            }
            
            gps_center_path.poses[i].pose.position.z = rs_;
        }

        // for(const auto &p:gps_center_path.poses)
        // { 
        //     std::cout << p.pose.position.x << '\t' << p.pose.position.y << '\t' << p.pose.position.z << '\t' << p.pose.orientation.z << std::endl;
        // }
        // std::cout << "finish route" << std::endl;
    }

    void FrenetCoordinateHandle::find_rs(double x, double y)
    {
        int gps_size = gps_center_path.poses.size();
        double min_dis = 9999;
        int flag_min_index = 0;

        for(int i = 0; i < gps_size; i++)
        {
            double distance =  std::hypot(x - gps_center_path.poses[i].pose.position.x, y - gps_center_path.poses[i].pose.position.y);
            if(distance < min_dis)
            {
                min_dis = distance;
                flag_min_index = i;
            }
        }

        double flag_rs = gps_center_path.poses[flag_min_index].pose.position.z;
        double flag_rx = gps_center_path.poses[flag_min_index].pose.position.x;
        double flag_ry = gps_center_path.poses[flag_min_index].pose.position.y;
        double flag_rtheta = gps_center_path.poses[flag_min_index].pose.orientation.z;
        cartesian_to_frenet(flag_rs,flag_rx,flag_ry,flag_rtheta,x,y);
    }
    
    void FrenetCoordinateHandle::cartesian_to_frenet(double rs, double rx, double ry, double rtheta, double x, double y)
    {
        double dx = x - rx;
        double dy = y - ry;
        double cos_theta_r = cos(rtheta);
        double sin_theta_r = sin(rtheta);
        double cross_rd_nd = cos_theta_r * dy - sin_theta_r * dx;
        double cone_d_ = copysign(sqrt(dx * dx + dy * dy), cross_rd_nd);
        double cone_s_ = rs;
        if(cone_d_ > 0)
        {
            left_cone_frenet.insert(std::pair <double, frenetcone> (cone_s_, frenetcone(x,y,cone_d_)));
        }
        else
        {
            right_cone_frenet.insert(std::pair <double, frenetcone> (cone_s_, frenetcone(x,y,cone_d_)));
        }
    }

    void FrenetCoordinateHandle::frenet_to_cartesian(double rx,double ry,double rtheta,double s_condition,double d_condition)
    {
        double cos_theta_r = cos(rtheta);
        double sin_theta_r = sin(rtheta);
        
        double x = rx - sin_theta_r * d_condition;
        double y = ry + cos_theta_r * d_condition;

        geometry_msgs::PoseStamped p;
        p.pose.position.x = x;
        p.pose.position.y = y;
        global_center_path.poses.push_back(p);
    }

    void FrenetCoordinateHandle::cal_center_path_frenet()
    {
        for(std::map<double, frenetcone>::iterator it = left_cone_frenet.begin(); it != left_cone_frenet.end(); it++)
        {
            frenetcone point = it->second;
            geometry_msgs::Point32 t;
            t.x = it->first;
            t.y = point.d;
            left_cone_.polygon.points.push_back(t);
            visiual_marker_array_CUBE(Temp_1,"world","r", t.x, t.y, 0);
        }

        for(std::map<double, frenetcone>::iterator it = right_cone_frenet.begin(); it != right_cone_frenet.end(); it++)
        {
            frenetcone point = it->second;
            geometry_msgs::Point32 t;
            t.x = it->first;
            t.y = point.d;
            right_cone_.polygon.points.push_back(t);
            visiual_marker_array_CUBE(Temp_1,"world","b", t.x, t.y, 0);
        }

        std::cout << "left_cone_: " << left_cone_.polygon.points.size() << ", right_cone_: " << right_cone_.polygon.points.size() << std::endl;



        double zoom = 2;
        //左右桩通头尾对齐
        s_start = left_cone_.polygon.points[0].x <right_cone_.polygon.points[0].x ? right_cone_.polygon.points[0].x :  left_cone_.polygon.points[0].x;
        s_end = left_cone_.polygon.points[left_cone_.polygon.points.size() - 1].x < right_cone_.polygon.points[right_cone_.polygon.points.size() - 1].x ? left_cone_.polygon.points[left_cone_.polygon.points.size() - 1].x : right_cone_.polygon.points[right_cone_.polygon.points.size() - 1].x;
        ROS_INFO("start: %f, end: %f", s_start, s_end);
        int n_origin = (s_end - s_start)  / 1 + 1;
        int n = int(zoom) * n_origin;
        //std::cout << "n: " << n << std::endl;
        double s[n] ={0};

        for(int i = 0; i < n; i++)
        {
            s[i] = s_start + (double(1 / zoom) * i);
            //std::cout << "i: " << i  << " " << "val: " << s[i] << " ";
        }
        //std::cout << std::endl;

        //计算样条插值
        ns_frenet_coordinate::Spline sp_left(left_cone_, left_cone_.polygon.points.size());
        sp_left.run_algorithm();

        ns_frenet_coordinate::Spline sp_right(right_cone_, right_cone_.polygon.points.size());
        sp_right.run_algorithm();

        //找对应样条线段
        int match_index_left = 1;
        int match_index_right = 1;

        for(int i = 0; i < n; i++)
        {
            for(int j = match_index_left; j < left_cone_.polygon.points.size(); j++ )
            {
                if(s[i] >= left_cone_.polygon.points[j-1].x  && s[i] < left_cone_.polygon.points[j].x )
                {
                    match_index_left = j;
                    //ROS_INFO("%d left find %d", i, j);
                    break;
                }
            }

            for(int j = match_index_right; j < right_cone_.polygon.points.size(); j++ )
            {
                if(s[i] >= right_cone_.polygon.points[j-1].x  && s[i] < right_cone_.polygon.points[j].x )
                {
                    match_index_right = j;
                    //ROS_INFO("%d right find %d", i, j);
                    break;
                }
            }
            //带入样条插值
            double d_left = sp_left.cal_spline_course(match_index_left,s[i]);
            double d_right = sp_right.cal_spline_course(match_index_right,s[i]);

            geometry_msgs::Point32 t;
            t.x = s[i];
            t.y = d_left;
            visiual_marker_array_CUBE(Temp_2,"world","r", t.x, t.y, 0);
            //ROS_INFO("%f left added", s[i]);
            left_cone_sp.polygon.points.push_back(t);
            t.y = d_right;
            visiual_marker_array_CUBE(Temp_2,"world","b", t.x, t.y, 0);
            //ROS_INFO("%f right added", s[i]);
            right_cone_sp.polygon.points.push_back(t);
        }
        //计算frenet坐标下的中心线
        double width;
        for(int i = 0 ; i< n; i++)
        {
            geometry_msgs::Point32 t;
            t.x = s[i];
            t.y = (left_cone_sp.polygon.points[i].y + right_cone_sp.polygon.points[i].y ) / 2;
            width = (left_cone_sp.polygon.points[i].y - right_cone_sp.polygon.points[i].y ) / 2;
            global_center_line_frenet.polygon.points.push_back(t);
            road_width.data.push_back(width);
            visiual_marker_array_CUBE(Temp_2,"world","g", t.x, t.y, 0);
        }


        // std::ofstream write;
        // write.open("/home/aracing/lr.txt");
        // for(int i = 0 ; i < n; i++)
        // {
        //     write << left_cone_sp.polygon.points[i].x << '\t';
        //     write << left_cone_sp.polygon.points[i].y << '\t';
        //     write << right_cone_sp.polygon.points[i].x << '\t';
        //     write << right_cone_sp.polygon.points[i].y << '\t';
        //     write << global_center_line_frenet.polygon.points[i].x << '\t';
        //     write << global_center_line_frenet.polygon.points[i].y << '\t';
        //     write << '\n';
        //     // std::cout << "left_s" << '\t' << left_cone_sp.polygon.points[i].x << '\t' <<  "left_y" << '\t' << left_cone_sp.polygon.points[i].y << '\t'
        //     //                     <<"right_s" << '\t' << right_cone_sp.polygon.points[i].x << '\t' <<  "right_y" << '\t' << right_cone_sp.polygon.points[i].y << '\t'
        //     //                     << "center_s" << '\t' << global_center_line_frenet.polygon.points[i].x << '\t' <<  "center_y" << '\t' << global_center_line_frenet.polygon.points[i].y << std::endl;
        // }
        // write.close();
        // std::cout << "\nwrite complete\n" << std::endl;
    }

    void FrenetCoordinateHandle::visiual_marker_array_CUBE(visualization_msgs::MarkerArray & marker_array, std::string frame_id_, std::string color, 
                                                            double x, double y, double z)
    {
        visualization_msgs::Marker bbox_marker;
        bbox_marker.header.frame_id = frame_id_;
        bbox_marker.header.stamp = ros::Time::now();
        bbox_marker.lifetime = ros::Duration();
        bbox_marker.frame_locked = true;
        bbox_marker.type = visualization_msgs::Marker::CUBE;
        bbox_marker.action = visualization_msgs::Marker::ADD;
        if(color == "r")
        {
            bbox_marker.color.r = 1.0f;
            bbox_marker.color.g = 0.0f;
            bbox_marker.color.b = 0.0f;
            bbox_marker.color.a = 1.0f;
        }
        if(color == "g")
        {
            bbox_marker.color.r = 0.0f;
            bbox_marker.color.g = 1.0f;
            bbox_marker.color.b = 0.0f;
            bbox_marker.color.a = 1.0f;
        }
        if(color == "b")
        {
            bbox_marker.color.r = 0.0f;
            bbox_marker.color.g = 0.0f;
            bbox_marker.color.b = 1.0f;
            bbox_marker.color.a = 1.0f;
        }
        bbox_marker.scale.x = 0.25;
        bbox_marker.scale.y = 0.25;
        bbox_marker.scale.z = 0.25;
        // default val to fix warn of rviz
        bbox_marker.pose.orientation.x = 0;
        bbox_marker.pose.orientation.y = 0;
        bbox_marker.pose.orientation.z = 0;
        bbox_marker.pose.orientation.w = 1;
        bbox_marker.id = marker_array.markers.size();
        bbox_marker.pose.position.x = x;
        bbox_marker.pose.position.y = y;
        bbox_marker.pose.position.z = z;
        marker_array.markers.push_back(bbox_marker);
    }

    void FrenetCoordinateHandle::visiual_marker_array_ARROW(visualization_msgs::MarkerArray & marker_array, std::string frame_id_, std::string color, 
                                                            double x1, double y1, double z1, double x2, double y2, double z2)
    {
        visualization_msgs::Marker bbox_marker;
        bbox_marker.header.frame_id = frame_id_;
        bbox_marker.header.stamp = ros::Time::now();
        bbox_marker.lifetime = ros::Duration();
        bbox_marker.frame_locked = true;
        bbox_marker.type = visualization_msgs::Marker::ARROW;
        bbox_marker.action = visualization_msgs::Marker::ADD;
        if(color == "r")
        {
            bbox_marker.color.r = 1.0f;
            bbox_marker.color.g = 0.0f;
            bbox_marker.color.b = 0.0f;
            bbox_marker.color.a = 1.0f;
        }
        if(color == "g")
        {
            bbox_marker.color.r = 0.0f;
            bbox_marker.color.g = 1.0f;
            bbox_marker.color.b = 0.0f;
            bbox_marker.color.a = 1.0f;
        }
        if(color == "b")
        {
            bbox_marker.color.r = 0.0f;
            bbox_marker.color.g = 0.0f;
            bbox_marker.color.b = 1.0f;
            bbox_marker.color.a = 1.0f;
        }
        bbox_marker.scale.x = 0.05;
        bbox_marker.scale.y = 0.08;
        bbox_marker.scale.z = 0.1;
        bbox_marker.id = marker_array.markers.size();
        geometry_msgs::Point p1, p2;
        p1.x = x1;
        p1.y = y1;
        p1.z = z1;
        p2.x = x1 + x2;
        p2.y = y1 + y2;
        p2.z = z1 + z2;
        bbox_marker.points.push_back(p1) ;
        bbox_marker.points.push_back(p2) ;
        marker_array.markers.push_back(bbox_marker);
    }

    void FrenetCoordinateHandle::discrete_center_line()
    {
        int global_center_path_path_size = global_center_path.poses.size();
        fsd_common_msgs::Cone slam_temp;
        for (size_t i = 0; i < global_center_path_path_size-1; i++)
        {
            
            double x1 = global_center_path.poses[i].pose.position.x,
                   x2 = global_center_path.poses[i+1].pose.position.x,
                   y1 = global_center_path.poses[i].pose.position.y,
                   y2 = global_center_path.poses[i+1].pose.position.y;
        
            double delta_x = x2 - x1,
                  delta_y = y2 - y1,
                  normal_dis = sqrt(delta_x * delta_x + delta_y * delta_y);
        
            double xx = delta_y * discrete_dis_ / normal_dis,
                  yy = -delta_x * discrete_dis_ / normal_dis;
            
            slam_temp.position.x = x1 + xx;
            slam_temp.position.y = y1 + yy;
            map_for_ribbon.cone_detections.push_back(slam_temp);
            map_for_ribbon_right.cone_detections.push_back(slam_temp);
            visiual_marker_array_CUBE(marker_array_discrete_line,"world","b",slam_temp.position.x,slam_temp.position.y,0);

            slam_temp.position.x = x1 - xx;
            slam_temp.position.y = y1 - yy;
            map_for_ribbon.cone_detections.push_back(slam_temp);
            map_for_ribbon_left.cone_detections.push_back(slam_temp);
            visiual_marker_array_CUBE(marker_array_discrete_line,"world","r",slam_temp.position.x,slam_temp.position.y,0);

        }
        double x1 = global_center_path.poses[global_center_path_path_size-1].pose.position.x,
               x2 = global_center_path.poses[0].pose.position.x,
               y1 = global_center_path.poses[global_center_path_path_size-1].pose.position.y,
               y2 = global_center_path.poses[0].pose.position.y;

        double delta_x = x2 - x1,
              delta_y = y2 - y1,
              normal_dis = sqrt(delta_x * delta_x + delta_y * delta_y);

        double xx = delta_y * discrete_dis_ / normal_dis,
              yy = -delta_x * discrete_dis_ / normal_dis;

        slam_temp.position.x = x1 + xx;
        slam_temp.position.y = y1 + yy;
        map_for_ribbon.cone_detections.push_back(slam_temp);
        map_for_ribbon_right.cone_detections.push_back(slam_temp);
        visiual_marker_array_CUBE(marker_array_discrete_line,"world","b",slam_temp.position.x,slam_temp.position.y,0);

        slam_temp.position.x = x1 - xx;
        slam_temp.position.y = y1 - yy;
        map_for_ribbon.cone_detections.push_back(slam_temp);
        map_for_ribbon_left.cone_detections.push_back(slam_temp);
        visiual_marker_array_CUBE(marker_array_discrete_line,"world","r",slam_temp.position.x,slam_temp.position.y,0);
 
        // std::ofstream write;
        // write.open("/home/automan/WorkingPlaces/for_ribbon.txt");
        // for(int i = 0 ; i < global_center_path_path_size; i++)
        // {
        //     write << marker_array_discrete_line.markers[i].pose.position.x << ",";
        //     write << marker_array_discrete_line.markers[i].pose.position.y;
        //     write << '\n';
        //     // std::cout << "left_s" << '\t' << left_cone_sp.polygon.points[i].x << '\t' <<  "left_y" << '\t' << left_cone_sp.polygon.points[i].y << '\t'
        //     //                     <<"right_s" << '\t' << right_cone_sp.polygon.points[i].x << '\t' <<  "right_y" << '\t' << right_cone_sp.polygon.points[i].y << '\t'
        //     //                     << "center_s" << '\t' << global_center_line_frenet.polygon.points[i].x << '\t' <<  "center_y" << '\t' << global_center_line_frenet.polygon.points[i].y << std::endl;
        // }
        // write.close();
        // std::cout << "\nwrite complete\n" << std::endl;
    }

    //对离散结果进行评价
    double FrenetCoordinateHandle::evaluate_discrete_map()
    {
        
        double n_map_for_ribbon = map_for_ribbon.cone_detections.size(), n_map_from_slam = map_from_slam.cone_detections.size();
        std::cout << "map_for_ribbon: " << n_map_for_ribbon << ", map_from_slam: " << n_map_from_slam << std::endl;

        //距离偏差
        for (size_t i = 0; i < n_map_from_slam; i++)
        {
            double min_dis = 99999999.0;
            for (size_t j = 0; j < n_map_for_ribbon; j++)
            {
                double dis_temp = sqrt(pow(map_for_ribbon.cone_detections[j].position.x - map_from_slam.cone_detections[i].position.x, 2) +
                                      pow(map_for_ribbon.cone_detections[j].position.y - map_from_slam.cone_detections[i].position.y, 2));
                min_dis = dis_temp > min_dis ? min_dis : dis_temp;
            }
            //std::cout << "min_dis: " << min_dis << std::endl;
            dis_evaluate_map += min_dis;
        }
        dis_evaluate_map = dis_evaluate_map / n_map_from_slam;

        //形心偏差
        int n_map_from_slam_left = map_from_slam_left.cone_detections.size(), n_map_from_slam_right = map_from_slam_right.cone_detections.size();
        int n_map_for_ribbon_left = map_for_ribbon_left.cone_detections.size(), n_map_for_ribbon_right = map_for_ribbon_right.cone_detections.size();
        std::cout << "n_map_from_slam_left: " << n_map_from_slam_left << ", n_map_from_slam_right: " << n_map_from_slam_right << std::endl;
        std::cout << "n_map_for_ribbon_left: " << n_map_for_ribbon_left << ", n_map_for_ribbon_right: " << n_map_for_ribbon_right << std::endl;
        std::cout << std::endl;

        double cx_map_from_slam_left = 0, cy_map_from_slam_left = 0, A_map_from_slam_left = 0, 
              cx_map_from_slam_right = 0, cy_map_from_slam_right = 0, A_map_from_slam_right = 0, 
              cx_map_for_ribbon_left = 0, cy_map_for_ribbon_left = 0, A_map_for_ribbon_left = 0, 
              cx_map_for_ribbon_right = 0, cy_map_for_ribbon_right = 0, A_map_for_ribbon_right = 0;
        
        calculate_center(map_from_slam_left, n_map_from_slam_left, 1.0, 0,
                         cx_map_from_slam_left, cy_map_from_slam_left, A_map_from_slam_left);
        std::cout << "cx_map_from_slam_left: " << cx_map_from_slam_left << " cy_map_from_slam_left: " << cy_map_from_slam_left 
                  << " A_map_from_slam_left: " << A_map_from_slam_left << std::endl;
        
        calculate_center(map_from_slam_right, n_map_from_slam_right, -1.0, 1,
                         cx_map_from_slam_right, cy_map_from_slam_right, A_map_from_slam_right);
        std::cout << "cx_map_from_slam_right: " << cx_map_from_slam_right << " cy_map_from_slam_right: " << cy_map_from_slam_right 
                  << " A_map_from_slam_right: " << A_map_from_slam_right << std::endl;
        
        calculate_center(map_for_ribbon_left, n_map_for_ribbon_left, 1.0, 2,
                         cx_map_for_ribbon_left, cy_map_for_ribbon_left, A_map_for_ribbon_left);
        std::cout << "cx_map_for_ribbon_left: " << cx_map_for_ribbon_left << " cy_map_for_ribbon_left: " << cy_map_for_ribbon_left 
                  << " A_map_for_ribbon_left: " << A_map_for_ribbon_left << std::endl;
        
        calculate_center(map_for_ribbon_right, n_map_for_ribbon_right, -1.0, 2,
                         cx_map_for_ribbon_right, cy_map_for_ribbon_right, A_map_for_ribbon_right);
        std::cout << "cx_map_for_ribbon_right: " << cx_map_for_ribbon_right << " cy_map_for_ribbon_right: " << cy_map_for_ribbon_right 
                  << " A_map_for_ribbon_right: " << A_map_for_ribbon_right << std::endl;
        std::cout << std::endl;

        double A_map_from_slam = A_map_from_slam_left + A_map_from_slam_right;
        double A_map_for_ribbon = A_map_for_ribbon_left + A_map_for_ribbon_right;
        double cx_map_from_slam = (cx_map_from_slam_left*A_map_from_slam_left + cx_map_from_slam_right*A_map_from_slam_right) / A_map_from_slam;
        double cy_map_from_slam = (cy_map_from_slam_left*A_map_from_slam_left + cy_map_from_slam_right*A_map_from_slam_right) / A_map_from_slam;
        double cx_map_for_ribbon = (cx_map_for_ribbon_left*A_map_for_ribbon_left + cx_map_for_ribbon_right*A_map_for_ribbon_right) / A_map_for_ribbon;
        double cy_map_for_ribbon = (cy_map_for_ribbon_left*A_map_for_ribbon_left + cy_map_for_ribbon_right*A_map_for_ribbon_right) / A_map_for_ribbon;
        
        std::cout << "cx_map_from_slam: " << cx_map_from_slam << " cy_map_from_slam: " << cy_map_from_slam << std::endl;
        std::cout << "cx_map_for_ribbon: " << cx_map_for_ribbon << " cy_map_for_ribbon: " << cy_map_for_ribbon << std::endl;
        std::cout << "A_map_from_slam: " << A_map_from_slam << " A_map_for_ribbon: " << A_map_for_ribbon << std::endl;
        
        std::cout << std::endl;
        centre_evaluate_map = sqrt(pow(cx_map_from_slam - cx_map_for_ribbon,2) + pow(cy_map_from_slam - cy_map_for_ribbon,2));
        area_evaluate_map = sqrt(abs(A_map_from_slam - A_map_for_ribbon));
        std::cout << "dis_evaluate_map: " << dis_evaluate_map 
                  << ", centre_evaluate_map: " << centre_evaluate_map 
                  << ", area_evaluate_map: " << area_evaluate_map << std::endl;
        std::cout << std::endl;
        double final_evaluate = dis_evaluation_weight_*dis_evaluate_map + centre_evaluation_weight_* centre_evaluate_map;
        std::cout << "final_evaluate: " << final_evaluate << std::endl;
        /*
        if ( final_evaluate < evaluation_threshold_)
        {
            std::cout << "I will choose Map_For_Ribbon !!" << std::endl;
            return 0;
        }
        else
        {
            std::cout << "I will choose Map_From_Slam !!" << std::endl;
            return 1;
        }
        */
       std::cout << "I will choose Map_From_Slam !!" << std::endl;
        return 1;
    }

    void FrenetCoordinateHandle::calculate_center(fsd_common_msgs::ConeDetections & map, int map_size, double in_or_out, double flag_which,
                                                  double & cx, double & cy, double & A)
    {
        std::cout << std::endl;
        double init_x = 99999999.0, init_y = 0.0;
        for (size_t i = 0; i < map_size; i++)
        {
            if(init_x > map.cone_detections[i].position.x)
            {
                init_x = map.cone_detections[i].position.x;
                init_y = map.cone_detections[i].position.y;
            }
        }

        double polar_coordinates[map_size][4] = {};
        for (size_t i = 0; i < map_size; i++)
        {
            double vector_x = map.cone_detections[i].position.x - init_x, vector_y = map.cone_detections[i].position.y - init_y;
            if(vector_x == 0 && vector_x == 0)
            {
                polar_coordinates[i][0] = -9999999.0;
                polar_coordinates[i][1] = -9999999.0;
                polar_coordinates[i][2] = -9999999.0;
                polar_coordinates[i][3] = -9999999.0;
                continue;
            }
            polar_coordinates[i][0] = atan2(vector_y, vector_x);
            polar_coordinates[i][1] = sqrt(pow(vector_x, 2) + pow(vector_y, 2));
            polar_coordinates[i][2] = map.cone_detections[i].position.x;
            polar_coordinates[i][3] = map.cone_detections[i].position.y;
        }

        for (size_t i = 0; i < map_size-1; i++)
        {
            for (size_t j = 0; j < map_size - 1 - i; j++)
            {
                if(polar_coordinates[j][0] < polar_coordinates[j+1][0])
                {
                    double temp = polar_coordinates[j+1][0];
                    polar_coordinates[j+1][0] = polar_coordinates[j][0];
                    polar_coordinates[j][0] = temp;
                    
                    temp = polar_coordinates[j+1][1];
                    polar_coordinates[j+1][1] = polar_coordinates[j][1];
                    polar_coordinates[j][1] = temp;

                    temp = polar_coordinates[j+1][2];
                    polar_coordinates[j+1][2] = polar_coordinates[j][2];
                    polar_coordinates[j][2] = temp;

                    temp = polar_coordinates[j+1][3];
                    polar_coordinates[j+1][3] = polar_coordinates[j][3];
                    polar_coordinates[j][3] = temp;
                }
            }
        }

        if(flag_which == 0)
        {
            std::cout << "Will Copy Data To map_from_slam_left_ordered" << std::endl;
            fsd_common_msgs::Cone slam_temp;
            slam_temp.position.x = init_x;
            slam_temp.position.y = init_y;
            map_from_slam_left_ordered.cone_detections.push_back(slam_temp);
            for (size_t i = 0; i < map_size-1; i++)
            {
                //std::cout << i << std::endl;
                slam_temp.position.x = polar_coordinates[i][2];
                slam_temp.position.y = polar_coordinates[i][3];
                map_from_slam_left_ordered.cone_detections.push_back(slam_temp);
            }
            std::cout << "map_from_slam_left_ordered: " << map_from_slam_left_ordered.cone_detections.size() << std::endl;
        }

        if(flag_which == 1)
        {
            std::cout << "Will Copy Data To map_from_slam_right_ordered" << std::endl;
            fsd_common_msgs::Cone slam_temp;
            slam_temp.position.x = init_x;
            slam_temp.position.y = init_y;
            map_from_slam_right_ordered.cone_detections.push_back(slam_temp);
            for (size_t i = 0; i < map_size-1; i++)
            {
                slam_temp.position.x = polar_coordinates[i][2];
                slam_temp.position.y = polar_coordinates[i][3];
                map_from_slam_right_ordered.cone_detections.push_back(slam_temp);
            }
            std::cout << "map_from_slam_right_ordered: " << map_from_slam_right_ordered.cone_detections.size() << std::endl;
        }

        double center[map_size-2][2] = {}, area[map_size-2] = {};
        for (size_t i = 0; i < map_size - 2; i++)
        {
            double x2 = polar_coordinates[i][2], y2 = polar_coordinates[i][3], 
                  x3 = polar_coordinates[i+1][2], y3 = polar_coordinates[i+1][3];
            center[i][0] = (init_x + x2 + x3) / 3, center[i][1] = (init_y + y2 + y3) / 3;
            area[i] = in_or_out*((x2 - init_x)*(y3 - init_y) - (x3 - init_x)*(y2 - init_y)) / 2;
        }

        for (size_t i = 0; i < map_size - 2; i++)
        {
            cx += center[i][0] * area[i];
            cy += center[i][1] * area[i];
            A += area[i];
        }
        cx = cx / A;
        cy = cy / A;
        
    }

    void FrenetCoordinateHandle::deal_global_center_path(int flag_choose_what)
    {
        int global_center_path_path_size = global_center_path.poses.size();
        double reference_dis = sqrt(pow(global_center_path.poses[0].pose.position.x - global_center_path.poses[1].pose.position.x,2) + 
                                    pow(global_center_path.poses[0].pose.position.y - global_center_path.poses[1].pose.position.y,2));
        
        double start_point_x = global_center_path.poses[0].pose.position.x, 
                start_point_y = global_center_path.poses[0].pose.position.y;
        double end_point_x = global_center_path.poses[global_center_path_path_size - 1].pose.position.x, 
                end_point_y = global_center_path.poses[global_center_path_path_size - 1].pose.position.y;
        double start_end_dis = sqrt(pow(start_point_x - end_point_x,2) + pow(start_point_y - end_point_y,2));
        std::cout << "reference_dis: " << reference_dis << ", start_end_dis: " << start_end_dis << std::endl;
        std::cout << "start: [" << start_point_x << ", " << start_point_y << "]" << std::endl;
        std::cout << "end: [" << end_point_x << ", " << end_point_y << "]" << std::endl;

        geometry_msgs::PoseStamped p;
        if(start_end_dis > 0 && start_end_dis < (reference_dis * 2))
        {
            std::cout << "Don't need any replenish !!" << std::endl;
        }
        else if(start_end_dis >= (reference_dis * 2) && start_end_dis <= (reference_dis * 4))
        {
            std::cout << "So close, Just need to make Linear Interpolation !!" << std::endl;
            p.pose.position.x = (start_point_x + end_point_x) / 2;
            p.pose.position.y = (start_point_y + end_point_y) / 2;
            global_center_path.poses.push_back(p);
        }
        else if(start_end_dis > (reference_dis * 4) && flag_choose_what == 0)
        {
            std::cout << "So far, Need To Use Curves !!" << std::endl;
            double k1 = (global_center_path.poses[2].pose.position.y - start_point_y) / (global_center_path.poses[2].pose.position.x - start_point_x);
            double k2 = (end_point_y - global_center_path.poses[global_center_path_path_size - 3].pose.position.y) / 
                        (end_point_x - global_center_path.poses[global_center_path_path_size - 3].pose.position.x);
            
            double a = 0.0, b = 0.0, c = 0.0, d = 0.0;
            LinearEquations4(start_point_x, end_point_x, start_point_y, end_point_y, k1, k2, a, b, c, d);

            double xx = end_point_x;
            while(xx > start_point_x)
            {
                visiual_marker_array_CUBE(Temp_1,"world","r", xx, CurveFunction(a, b, c, d, xx), 0);
                xx -= 0.1;
            }
            visiual_marker_array_CUBE(Temp_1,"world","b", start_point_x, start_point_y, 0);
            visiual_marker_array_CUBE(Temp_1,"world","b", global_center_path.poses[2].pose.position.x, global_center_path.poses[2].pose.position.y, 0);
            visiual_marker_array_CUBE(Temp_1,"world","b", end_point_x, end_point_y, 0);
            visiual_marker_array_CUBE(Temp_1,"world","b", global_center_path.poses[global_center_path_path_size - 3].pose.position.x, global_center_path.poses[global_center_path_path_size - 3].pose.position.y, 0);

            std::cout << "a: " << a << ", b: " << b << ", c: " << c << ", d: " << d << ", k1: " << k1 << ", k2: " << k2 << std::endl;
            std::cout << "1: " << start_point_y - CurveFunction(a, b, c, d, start_point_x) << std::endl;
            std::cout << "2: " << end_point_y - CurveFunction(a, b, c, d, end_point_x) << std::endl;
            std::cout << "3: " << k1 - DiffFunction(a, b, c, d, start_point_x) << std::endl;
            std::cout << "4: " << k2 - DiffFunction(a, b, c, d, end_point_x) << std::endl;

            double delta_x = reference_dis / 10.0, x = end_point_x, y, x_last = end_point_x, y_last = end_point_y;
            double direction_x = (start_point_x - end_point_x) / abs(end_point_x - start_point_x);
            std::cout << "delta_x: " << delta_x << ", direction_x: " << direction_x << std::endl;
            int add_point = 0;
            bool ok = 1;
            do
            {
                x += direction_x * delta_x;

                if(x == start_point_x)
                {
                    ok = 0;
                }
                else if(direction_x < 0)
                {
                    ok = x < start_point_x ? 0 : 1;
                }
                else if(direction_x > 0)
                {
                    ok = x > start_point_x ? 0 : 1;
                }
                if(!ok) {break;}

                y = CurveFunction(a, b, c, d, x);
                double delta_xy = sqrt(pow(x_last - x,2) + pow(y_last - y,2));
                if(delta_xy >= reference_dis)
                {
                    p.pose.position.x = x;
                    p.pose.position.y = y;
                    global_center_path.poses.push_back(p);
                    add_point++;
                    std::cout << "ADD: [" << x << ", " << y << "]" << "delta_xy: " << delta_xy << std::endl;

                    x_last = x;
                    y_last = y;
                }
                
            } while (1);
            std::cout << "ADD " << add_point-1 << " points in global_center_path" << std::endl;
        }
        else if(start_end_dis > (reference_dis * 4) && flag_choose_what == 1)
        {
            std::cout << "So far, Need To Use VariableSlope !!" << std::endl;
            double k1 = (global_center_path.poses[1].pose.position.y - start_point_y) / (global_center_path.poses[1].pose.position.x - start_point_x);
            double k2 = (end_point_y - global_center_path.poses[global_center_path_path_size - 2].pose.position.y) / 
                        (end_point_x - global_center_path.poses[global_center_path_path_size - 2].pose.position.x);
            std::cout << "k1: " << k1 << ", k2:" << k2 << std::endl;

            int add_points_num = (int)(start_end_dis / (reference_dis * 1)) - 1;
            std::cout << "add_points_num: " << add_points_num << std::endl;
            double add_points[add_points_num][2] = {};

            double k_counts[add_points_num+2][2] = {};
            k_counts[0][0] = end_point_x - global_center_path.poses[global_center_path_path_size - 2].pose.position.x;
            k_counts[0][1] = end_point_y - global_center_path.poses[global_center_path_path_size - 2].pose.position.y;
            k_counts[add_points_num+1][0] = global_center_path.poses[1].pose.position.x - start_point_x;
            k_counts[add_points_num+1][1] = global_center_path.poses[1].pose.position.y - start_point_y;
            
            if (end_point_x < start_point_x)
            {
                double temp_rate = change_angle_rate_;
                std::cout << "Change the end_point in clockwise: " << temp_rate << std::endl;
                double temp = pow(k_counts[0][0],2) + pow(k_counts[0][1],2);
                k_counts[0][0] = k_counts[0][0] * temp_rate;
                k_counts[0][1] = k_counts[0][1] / abs(k_counts[0][1]) * sqrt(temp - k_counts[0][0] * k_counts[0][0]);
            }
            if (end_point_x > start_point_x)
            {
                double temp_rate = 2 - change_angle_rate_;
                std::cout << "Change the end_point in anticlockwise: " << temp_rate << std::endl;
                double temp = pow(k_counts[0][0],2) + pow(k_counts[0][1],2);
                k_counts[0][0] = k_counts[0][0] * temp_rate;
                k_counts[0][1] = k_counts[0][1] / abs(k_counts[0][1]) * sqrt(temp - k_counts[0][0] * k_counts[0][0]);
            }
            
            std::cout << "k_counts: [" << k_counts[0][0] << ", " << k_counts[0][1] << "], k: " << k_counts[0][1]/k_counts[0][0] << std::endl;
            double delta_x = k_counts[add_points_num+1][0] - k_counts[0][0], delta_y = k_counts[add_points_num+1][1] - k_counts[0][1];
            for (size_t i = 1; i < add_points_num+1; i++)
            {
                k_counts[i][0] = delta_x / (double)(add_points_num + 1) * i + k_counts[0][0];
                k_counts[i][1] = delta_y / (double)(add_points_num + 1) * i + k_counts[0][1];
                std::cout << "k_counts: [" << k_counts[i][0] << ", " << k_counts[i][1] << "], k: " << k_counts[i][1]/k_counts[i][0] << std::endl;
            }
            std::cout << "k_counts: [" << k_counts[add_points_num+1][0] << ", " << k_counts[add_points_num+1][1] << "], k: " << k_counts[add_points_num+1][1]/k_counts[add_points_num+1][0] << std::endl;
            VariableSlopeInterpolation(start_point_x, end_point_x, start_point_y, end_point_y, k_counts, add_points, add_points_num, reference_dis);
            
            for (const auto & ap : add_points)
            {
                std::cout << "add_point: [" << ap[0] << ", " << ap[1] << "]" << std::endl;
                visiual_marker_array_CUBE(Temp_1,"world","r", ap[0], ap[1], 0);
            }
            
            for (size_t i = 0; i < add_points_num+2; i++)
            {
                if (i == 1)
                {
                    visiual_marker_array_ARROW(Temp_1,"world","b", end_point_x, end_point_y, 0, k_counts[i][0], k_counts[i][1], 0);
                }
                visiual_marker_array_ARROW(Temp_1,"world","b", add_points[i-1][0], add_points[i-1][1], 0, k_counts[i][0], k_counts[i][1], 0);
                if (i == add_points_num+1)
                {
                    visiual_marker_array_ARROW(Temp_1,"world","b", start_point_x, start_point_y, 0, k_counts[i][0], k_counts[i][1], 0);
                }
            }
        }
    }
    
    void FrenetCoordinateHandle::global_center_path_TO_visiual_marker_array_CUBE()
    {
        for(const auto & g_c_p : global_center_path.poses)
        {
            visiual_marker_array_CUBE(marker_array_center_line,"world","g", g_c_p.pose.position.x, g_c_p.pose.position.y, 0);
        }
    }

    void FrenetCoordinateHandle::LinearEquations4(double x1, double x2, double y1, double y2, double k1, double k2, double & a, double & b, double & c, double & d)
    {
        const int num = 4;
        double AugmentedMatrix[num][num+1] = {pow(x1,3), pow(x1,2), x1, 1, y1, 
                                        pow(x2,3), pow(x2,2), x2, 1, y2, 
                                        3 * pow(x1,2), 2 * x1, 1, 0, k1, 
                                        3 * pow(x2,2), 2 * x2, 1, 0, k2};
        
        // for (size_t i = 0; i < num; i++)
        // {
        //     for (size_t j = 0; j < num+1; j++)
        //     {
        //         std::cout << AugmentedMatrix[i][j] << " ";
        //     }
        //     std::cout << std::endl;
        // }

        double abcde[num] = {}, delta = 0.0;
        for (size_t i = 0; i < num; i++)
        {
            for (size_t j = i+1; j < num; j++)
            {
                double temp = AugmentedMatrix[j][i];
                for (size_t k = i; k < num+1; k++)
                {
                    if (k == i)
                    {
                        AugmentedMatrix[j][k] = 0.0;
                        continue;
                    }
                    
                    AugmentedMatrix[j][k] = AugmentedMatrix[j][k] * AugmentedMatrix[i][i] / temp - AugmentedMatrix[i][k];
                }
            }
        }

        // std::cout << std::endl;
        // for (size_t i = 0; i < num; i++)
        // {
        //     for (size_t j = 0; j < num+1; j++)
        //     {
        //         std::cout << AugmentedMatrix[i][j] << " ";
        //     }
        //     std::cout << std::endl;
        // }

        //顶不住了，一条一条写吧
        abcde[3] = AugmentedMatrix[3][4] / AugmentedMatrix[3][3];
        abcde[2] = (AugmentedMatrix[2][4] - AugmentedMatrix[2][3] * abcde[3]) / AugmentedMatrix[2][2];
        abcde[1] = (AugmentedMatrix[1][4] - AugmentedMatrix[1][3] * abcde[3] - AugmentedMatrix[1][2] * abcde[2]) / AugmentedMatrix[1][1];
        abcde[0] = (AugmentedMatrix[0][4] - AugmentedMatrix[0][3] * abcde[3] - AugmentedMatrix[0][2] * abcde[2] - AugmentedMatrix[0][1] * abcde[1]) / AugmentedMatrix[0][0];
        
        a = abcde[0];
        b = abcde[1];
        c = abcde[2];
        d = abcde[3];

        // std::cout << "a: " << a << ", b: " << b << ", c: " << c << ", d: " << d << std::endl;

        // for (size_t i = 0; i < num; i++)
        // {
        //     delta = AugmentedMatrix[i][0] * abcde[0] + AugmentedMatrix[i][1] * abcde[1] + AugmentedMatrix[i][2] * abcde[2] + AugmentedMatrix[i][3] * abcde[3] - AugmentedMatrix[i][4]; 
        //     std::cout << "delta[" << i << "] = " << delta << std::endl;
        // }
        
        // for (size_t i = 0; i < num; i++)
        // {
        //     cout << delta[i] << " ";
        // }
        // cout << endl;
    }

    void FrenetCoordinateHandle::VariableSlopeInterpolation(double x1, double x2, double y1, double y2, double k_counts[][2], double add_points[][2], int add_points_num, double reference_dis)
    {

        double temp_x = x2, temp_y = y2;

        for(size_t i = 0; i < add_points_num; i++)
        {
            double cos_theda = (k_counts[i][0] * (x1 - x2) + k_counts[i][1] * (y1 - y2)) / 
                               (sqrt(pow(k_counts[i][0],2) + pow(k_counts[i][1],2)) * sqrt(pow((x1 - x2),2) + pow((y1 - y2),2)));
            double radian = change_reference_rate_ * reference_dis / cos_theda;
            std::cout << "cos_theda: " << cos_theda << ", radian: " << radian << std::endl;

            add_points[i][0] = temp_x + k_counts[i][0] / sqrt(k_counts[i][0] * k_counts[i][0] + k_counts[i][1] * k_counts[i][1]) * radian;
            add_points[i][1] = temp_y + k_counts[i][1] / sqrt(k_counts[i][0] * k_counts[i][0] + k_counts[i][1] * k_counts[i][1]) * radian;
            
            geometry_msgs::PoseStamped p;
            p.pose.position.x = add_points[i][0];
            p.pose.position.y = add_points[i][1];
            global_center_path.poses.push_back(p);

            temp_x = add_points[i][0];
            temp_y = add_points[i][1];
        }
    }

    double FrenetCoordinateHandle::CurveFunction(double a, double b, double c, double d, double x)
    {
        double result = a * pow(x,3) + b * pow(x,2) + c * x + d;
        return result;
    }

    double FrenetCoordinateHandle::DiffFunction(double a, double b, double c, double d, double x)
    {
        double result = 3 * a * pow(x,2) + 2 * b * x + c;
        return result;
    }

    
}

