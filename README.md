# SLAM
新版的 SLAM 模块
包含包 mapping 和修改过以适配 mapping 的包 frenet_coordinate

# 依赖
1. ros
1. fsd_common_msgs

# 构建
1. 将本仓库部署在包含`fsd_common_msgs`的 ros 工作空间内
1. `catkin build`或`catkin_make`